package com.isyscore.isc.mikilin.util;

import java.util.Collection;
import java.util.Map;

/**
 * 尽量不依赖外部的工具
 *
 * @author zhouzhenyong
 * @since 2019/1/5 下午1:21
 */
public class CollectionUtil {

    public static boolean isEmpty(Collection<?> collection) {
        return collection == null || collection.isEmpty();
    }

    public static boolean isEmpty(Map<?, ?> map) {
        return map == null || map.isEmpty();
    }
}
