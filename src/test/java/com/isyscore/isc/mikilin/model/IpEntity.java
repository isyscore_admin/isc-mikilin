package com.isyscore.isc.mikilin.model;

import com.isyscore.isc.mikilin.annotation.Matcher;
import com.isyscore.isc.mikilin.match.FieldModel;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author zhouzhenyong
 * @since 2019/3/10 下午9:51
 */
@Data
@Accessors(chain = true)
public class IpEntity {

    @Matcher(model = FieldModel.IP_ADDRESS)
    private String ipValid;
    @Matcher(model = FieldModel.IP_ADDRESS, accept = false)
    private String ipInvalid;
}
