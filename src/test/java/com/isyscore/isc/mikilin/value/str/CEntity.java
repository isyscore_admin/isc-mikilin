package com.isyscore.isc.mikilin.value.str;

import com.isyscore.isc.mikilin.annotation.Check;
import com.isyscore.isc.mikilin.annotation.Matcher;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * @author zhouzhenyong
 * @since 2019/1/5 下午7:05
 */
@Data
@Accessors(chain = true)
public class
CEntity {

    @Matcher({"a", "b"})
    private String name;
    @Check
    private List<BEntity> bEntities;
}
