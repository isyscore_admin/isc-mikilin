package com.isyscore.isc.mikilin.value.str;

import com.isyscore.isc.mikilin.annotation.Check;
import com.isyscore.isc.mikilin.annotation.Matcher;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author zhouzhenyong
 * @since 2019/1/5 下午6:44
 */
@Data
@Accessors(chain = true)
public class WhiteBEntity {

    @Check
    private BEntity bEntity;
    @Matcher({"a", "b"})
    private String name;
}
