package com.isyscore.isc.mikilin.check;

import com.isyscore.isc.mikilin.annotation.Matcher;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * @author shizi
 * @since 2021-03-31 16:18:49
 */
@Getter
@Setter
@EqualsAndHashCode(of = "name")
@AllArgsConstructor
public class CheckInnerEntity {

    @Matcher(range = "[0, 12]")
    private Integer age;
    private String name;
}
