package com.isyscore.isc.mikilin.model;

import com.isyscore.isc.mikilin.annotation.Matcher;
import com.isyscore.isc.mikilin.match.FieldModel;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author zhouzhenyong
 * @since 2019/3/10 下午9:49
 */
@Data
@Accessors(chain = true)
public class PhoneEntity {

    @Matcher(model = FieldModel.PHONE_NUM)
    private String phoneValid;
    @Matcher(model = FieldModel.PHONE_NUM, accept = false)
    private String phoneInValid;
}
