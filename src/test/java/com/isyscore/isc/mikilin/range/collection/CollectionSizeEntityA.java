package com.isyscore.isc.mikilin.range.collection;

import com.isyscore.isc.mikilin.annotation.Check;
import com.isyscore.isc.mikilin.annotation.Matcher;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * @author zhouzhenyong
 * @since 2019-09-15 21:24
 */
@Data
@Accessors(chain = true)
public class CollectionSizeEntityA {

    private String name;

    @Check
    @Matcher(range = "(0, 2]")
    private List<CollectionSizeEntityB> bList;
}
