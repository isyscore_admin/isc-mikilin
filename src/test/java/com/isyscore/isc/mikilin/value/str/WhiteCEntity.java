package com.isyscore.isc.mikilin.value.str;

import com.isyscore.isc.mikilin.annotation.Check;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * @author zhouzhenyong
 * @since 2019/1/5 下午7:04
 */
@Data
@Accessors(chain = true)
public class WhiteCEntity {

    @Check
    private List<CEntity> cEntities;
    @Check
    private BEntity bEntity;
}
