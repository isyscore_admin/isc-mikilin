package com.isyscore.isc.mikilin.value.str;

import com.isyscore.isc.mikilin.annotation.Matcher;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author shizi
 * @since 2020/4/2 3:40 PM
 */
@Data
@Accessors(chain = true)
public class StringValueEntity1 {

    /**
     * 只要空字符，则进行拒绝，其他的都不拦截
     */
    @Matcher(value = "c", accept = false, errMsg = "值#current是禁止的")
    private String emptyStr;
}
