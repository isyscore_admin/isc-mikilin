package com.isyscore.isc.mikilin.model;

import com.isyscore.isc.mikilin.annotation.Matcher;
import com.isyscore.isc.mikilin.match.FieldModel;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author zhouzhenyong
 * @since 2019/3/10 下午3:38
 */
@Data
@Accessors(chain = true)
public class IdCardEntity {

    @Matcher(model = FieldModel.ID_CARD)
    private String idCardValid;
}
