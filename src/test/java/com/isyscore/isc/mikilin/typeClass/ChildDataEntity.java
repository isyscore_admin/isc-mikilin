package com.isyscore.isc.mikilin.typeClass;

import com.isyscore.isc.mikilin.annotation.Matcher;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author shizi
 * @since 2020/3/24 下午6:50
 */
@Data
@Accessors(chain = true)
public class ChildDataEntity extends DataEntity {

    @Matcher(value = {"a", "b"})
    private String nameChild;
}
